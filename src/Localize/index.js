import I18n from 'react-native-i18n';
import memoize from 'lodash.memoize'; // Use for caching/memoize for better performance
import i18n from 'i18n-js';

const missingTranslationRegex = /^\[missing ".*" translation\]$/;

const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);


export default {
  ...I18n,
  t: translate,
};
