import React from 'react';
import {View, Image} from 'react-native';
import styles from './Styles/Banner';
import {Images} from '../Styles';

const Banner = props => {
  return (
    <View style={styles.container}>
      <Image source={Images.banner} style={styles.image} />
    </View>
  );
};

export default Banner;
