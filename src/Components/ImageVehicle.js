import React from 'react';
import {View, Image} from 'react-native';
import styles from './Styles/ImageVehicle';
import {Images} from '../Styles';

const ImageVehicle = props => {
  return (
    <View style={styles.container}>
      <Image
        source={Images.vehicle_toyota_corolla_1996_gray}
        style={styles.image}
      />
    </View>
  );
};

export default ImageVehicle;
