import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors} from '../../Styles';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.transparent,
    width: Metrics.screenWidth,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  full: {
    height: Metrics.screenHeight - Metrics.headerHeight * 2,
  },

  single: {
    height: Metrics.headerHeight,
  },

  text: {
    color: Colors.snow,
    textAlign: 'center',
  },

  fullText: {
    ...Fonts.style.h1,
  },

  singleText: {
    ...Fonts.style.h3,
  },

  image: {
    height: Metrics.headerHeight * 2,
    width: Metrics.headerHeight * 2,
  },
});
