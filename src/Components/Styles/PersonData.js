import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles';

//
export default StyleSheet.create({
  container: {
    marginTop: Metrics.marginVertical,
    flexDirection: 'column',
    justifyContent: 'center',
  },

  photoDataContainer: {
    flexDirection: 'row',
  },

  fullContainer: {
    flexDirection: 'column',
    width: Metrics.width,
  },

  dataContainer: {
    flexDirection: 'column',
  },

});
