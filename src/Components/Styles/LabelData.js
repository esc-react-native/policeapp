import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles';

//
export default StyleSheet.create({
  container: {
    height: Metrics.headerHeight,
    width: Metrics.screenWidth,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  borderBottom: {
    borderBottomColor: Colors.darkMidnight,
    borderBottomWidth: 3,
  },

  label: {
    color: Colors.grayLight,
    fontWeight: 'bold',
    fontSize: Fonts.size.h6,
  },

  textSmall: {
    fontSize: Fonts.size.h7,
    paddingHorizontal: Metrics.widthUnit
  },

  data: {
    color: Colors.text,
    fontSize: Fonts.size.h4,
  },

});
