import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles';

//
export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
});
