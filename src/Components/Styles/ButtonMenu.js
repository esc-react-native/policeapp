import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../Styles';

const FontSize = (Metrics.screenWidth - Metrics.headerHeight) / 10;

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.button,
    width: Metrics.screenWidth,
    height: Metrics.headerHeight,
    marginTop: Metrics.heightUnit,
    marginHorizontal: Metrics.widthUnit,
    justifyContent: 'center',
  },

  containerSmall: {
    height: (Metrics.headerHeight * 2) / 3,
  },

  button: {
    flexDirection: 'row',
  },

  imageContainer: {
    width: Metrics.headerHeight,
    height: Metrics.headerHeight,
    justifyContent: 'center',
    alignItems: 'center',
  },

  imageContainerSmall: {
    width: Metrics.headerHeight / 2,
    height: Metrics.headerHeight / 2,
    marginLeft: Metrics.marginHorizontal,
  },

  image: {
    width: Metrics.headerHeight * (2 / 3),
    height: Metrics.headerHeight * (2 / 3),
    resizeMode: 'stretch',
    backgroundColor: 'green',
  },

  imageSmall: {
    width: Metrics.headerHeight * (2 / 3),
  },

  text: {
    fontSize: FontSize,
    color: Colors.snow,
    alignSelf: 'center',
    paddingLeft: Metrics.marginHorizontalLarge,
    fontWeight: 'bold',
  },

  textSmall: {
    fontSize: (FontSize * 2) / 3,
    fontWeight: 'bold',
  },

  error: {
    height: 40,
    textAlign: 'center',
    fontSize: Fonts.size.small,
    color: Colors.error,
  },

  icon: {
    width: Metrics.icons.large,
    height: Metrics.icons.xl,
    resizeMode: 'stretch'
  },


});
