import {StyleSheet} from 'react-native';
import {Metrics} from '../../Styles';

//
export default StyleSheet.create({
  container: {
    height: Metrics.subtitleHeightMedium * 4,
    width: Metrics.screenWidth * (1 / 3),
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: Metrics.marginHorizontal,
  },

  fullContainer: {
    height: Metrics.subtitleHeightMedium * 7,
    width: Metrics.screenWidth,
    flexDirection: 'column',
    marginLeft: 0,
  },

  image: {
    height: Metrics.headerHeight * 2,
    width: Metrics.screenWidth * (1 / 3),
    padding: Metrics.marginHorizontalLarge,
    resizeMode: 'stretch',
  },

  fullImage: {
    height: Metrics.headerHeight * 3,
    width: Metrics.screenWidth * (6/10),
    padding: Metrics.marginHorizontalLarge,
    resizeMode: 'stretch',
  },
});
