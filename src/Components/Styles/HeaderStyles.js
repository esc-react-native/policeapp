import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics, AppStyles} from '../../Styles/index';

//
export default StyleSheet.create({
  titleContainer: {
    width: Metrics.screenWidth - Metrics.headerHeight,
    height: Metrics.headerHeight,
    flexDirection: 'column',
    justifyContent: 'center',
  },

  buttonContainer: {
    width: Metrics.headerHeight,
    height: Metrics.headerHeight,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: Colors.button,
  },

  title: {
    ...Fonts.style.h4,
    paddingHorizontal: Metrics.marginHorizontalLarge,
    color: Colors.text,
  },

  subtitle: {
    ...Fonts.style.h7,
    paddingHorizontal: Metrics.marginHorizontalLarge,
    color: Colors.text,
  },

  button: {
    width: Metrics.headerHeight,
    height: Metrics.headerHeight,
    flexDirection: 'row',
    justifyContent: 'center',
  },

  iconContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  icon: {
    ...AppStyles.icon,
  },

  iconText: {
    fontSize: Fonts.size.small,
    color: Colors.snow,
  },

});
