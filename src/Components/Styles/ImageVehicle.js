import {StyleSheet} from 'react-native';
import {Metrics} from '../../Styles';

//
export default StyleSheet.create({
  container: {
    height: Metrics.headerHeight * 2,
    width: Metrics.screenWidth,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  image: {
    height: (Metrics.headerHeight - Metrics.marginVertical) * 2,
    width: Metrics.screenWidth - Metrics.marginHorizontalLarge * 2,
    resizeMode: 'stretch',
  },
});
