import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../Styles';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.bluePolice,
    width: Metrics.screenWidth,
    height: Metrics.headerHeight,
  },

  image: {
    width: Metrics.screenWidth,
    height: Metrics.headerHeight,
    resizeMode: 'stretch',
  },

  error: {
    height: 40,
    textAlign: 'center',
    fontSize: Fonts.size.small,
    color: Colors.error,
  },
});
