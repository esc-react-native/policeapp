import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

//
export default StyleSheet.create({
  button: {
    marginVertical: Metrics.marginVertical,
    height: Metrics.componentHeigth,
    borderRadius: 10,
    justifyContent: 'center',
  },

  buttonOn: {
    backgroundColor: Colors.fire,
  },

  buttonOff: {
    backgroundColor: Colors.frost,
  },

  buttonText: {
    textAlign: 'center',
    color: Colors.snow,
    fontSize: Fonts.size.button,
    fontFamily: Fonts.type.bold,
  },
});
