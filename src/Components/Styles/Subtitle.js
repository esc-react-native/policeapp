import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles';

//
export default StyleSheet.create({
  container: {
    height: Metrics.headerHeight,
    width: Metrics.screenWidth,
    flexDirection: 'column',
    justifyContent: 'center',
  },

  ctLarge: {
    width: Metrics.screenWidth,
    height: 'auto',
  },

  ctMedium: {
    width: Metrics.screenWidth * (2 / 3),
    height: Metrics.headerHeight * (4 / 10),
  },

  ctSmall: {
    width: Metrics.screenWidth * (2 / 3),
    height: Metrics.headerHeight * (3 / 10),
  },

  borderBottom: {
    borderBottomColor: Colors.darkMidnight,
    borderBottomWidth: 3,
  },

  txSmall: {
    fontSize: Fonts.size.subtitleSmall,
  },

  txLarge: {
    fontSize: Fonts.size.subtitleLarge,
  },

  txMedium: {
    fontSize: Fonts.size.subtitleSmall,
  },


  text: {
    color: Colors.text,
    fontSize: Fonts.size.subtitleMedium,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
