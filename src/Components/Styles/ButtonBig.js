import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

//
export default StyleSheet.create({
  titleContainer: {
    marginTop: Metrics.marginVerticalLarge,
    height: Metrics.sectionHeightUnit * 2.5,
    flexDirection: 'column',
    justifyContent: 'center',
  },

  title: {
    ...Fonts.style.h1,
    color: Colors.textDark,
    paddingHorizontal: Metrics.marginHorizontalLarge,
  },

  inputContainer: {
    height: Metrics.sectionHeightUnit * 2,
    flexDirection: 'column',
    justifyContent: 'center',
    paddingBottom: Metrics.marginVerticalSmall,
    // backgroundColor: 'yellow'
  },

  inputLabel: {
    fontSize: Fonts.size.h6,
    color: Colors.textDark,
    marginHorizontal: Metrics.marginHorizontalLarge,
    // backgroundColor: 'blue',
  },

  input: {
    fontSize: Fonts.size.h5,
    color: Colors.textDark,
    marginHorizontal: Metrics.marginHorizontalLarge,
    borderBottomWidth: 1,
    // backgroundColor: 'green',
  },

  buttonContainer: {
    height: Metrics.sectionHeightUnit * 2,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    //backgroundColor: 'red'
  },

  button: {
    ...Fonts.style.h4,
    color: Colors.textDark,
    //paddingHorizontal: Metrics.marginHorizontalLarge,
    //width: Metrics.sectionWidth,
    justifyContent: 'center',
    //backgroundColor: 'blue'
  },

  buttonText: {
    ...Fonts.style.h1,
    fontSize: Fonts.size.h3,
    textAlign: 'right',
    color: Colors.textDark,
    paddingHorizontal: Metrics.marginHorizontalLarge,
  },

  buttonOn: {
    color: Colors.snow,
  },

  buttonOff: {
    color: Colors.coldGray,
    fontSize: Fonts.size.h4,
  },

  logo: {
    height: Metrics.sectionHeight / 2,
    width: Metrics.width,
    position: 'absolute',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    left: Metrics.marginHorizontalLarge,
    right: Metrics.marginHorizontalLarge,
    bottom: Metrics.marginVerticalLarge,
  },


  background: {
    height: Metrics.sectionHeight,
    resizeMode: 'stretch',
  },

});
