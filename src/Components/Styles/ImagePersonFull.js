import {StyleSheet} from 'react-native';
import {Metrics} from '../../Styles';

//
export default StyleSheet.create({
  container: {
    height: Metrics.subtitleHeightMedium * 4,
    width: Metrics.screenWidth * (1 / 3),
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    marginLeft: Metrics.marginHorizontal,
  },

  image: {
    height: Metrics.headerHeight * 2,
    width: Metrics.screenWidth * (1 / 3),
    padding: Metrics.marginHorizontalLarge,
    resizeMode: 'stretch',
  },
});
