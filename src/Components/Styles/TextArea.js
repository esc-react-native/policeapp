import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../Styles/index';

//
export default StyleSheet.create({
  inputContainer: {
    backgroundColor: Colors.transparent,
    width: Metrics.screenWidth,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  fullContainer: {
    height: Metrics.screenHeight - Metrics.headerHeight * 2,
  },

  singleContainer: {
    height: Metrics.headerHeight,
  },

  inputLabel: {
    fontSize: Fonts.size.h4,
    color: Colors.textLight,
    marginHorizontal: Metrics.marginHorizontalLarge,
    marginBottom: Metrics.marginHorizontalSmall,
    marginTop: Metrics.marginHorizontalSmall,
  },

  input: {
    fontSize: Fonts.size.h5,
    height: Metrics.sectionHeight / 2,
    color: Colors.textDark,
    marginHorizontal: Metrics.marginHorizontalLarge,
    width: Metrics.screenWidthNoMargin,
    textAlign: 'left',
    backgroundColor: Colors.snow,
    paddingHorizontal: Metrics.marginHorizontal,
  },

  buttonContainer: {
    height: Metrics.sectionHeightUnit * 2,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    //backgroundColor: 'red'
  },

  button: {
    ...Fonts.style.h4,
    color: Colors.textDark,
    //paddingHorizontal: Metrics.marginHorizontalLarge,
    //width: Metrics.sectionWidth,
    justifyContent: 'center',
    //backgroundColor: 'blue'
  },

  buttonText: {
    ...Fonts.style.h1,
    fontSize: Fonts.size.h3,
    textAlign: 'right',
    color: Colors.textDark,
    paddingHorizontal: Metrics.marginHorizontalLarge,
    //backgroundColor: 'green'
  },

  buttonOn: {
    color: Colors.textDark,
  },

  buttonOff: {
    color: Colors.coldGray,
    fontSize: Fonts.size.h4,
  },

  logo: {
    height: Metrics.sectionHeight / 2,
    width: Metrics.width,
    position: 'absolute',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    left: Metrics.marginHorizontalLarge,
    right: Metrics.marginHorizontalLarge,
    bottom: Metrics.marginVerticalLarge,
  },

  background: {
    height: Metrics.sectionHeight,
    resizeMode: 'stretch',
  },

});
