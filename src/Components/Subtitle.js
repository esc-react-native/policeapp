import React from 'react';
import {View, Text} from 'react-native';
import styles from './Styles/Subtitle';

const Subtitle = props => {
  let containerStyle = styles.container;
  let textStyle = styles.text;
  let borderStyle;

  if (props.borderBottom) {
    borderStyle = styles.borderBottom;
  }

  if (props.ctLarge) {
    containerStyle = styles.ctLarge;
    textStyle = styles.txLarge;
  }

  if (props.ctMedium) {
    containerStyle = styles.ctMedium;
    textStyle = styles.ctMedium;
  }

  if (props.ctSmall) {
    containerStyle = styles.ctSmall;
    textStyle = styles.txSmall;
  }

  if (props.txMedium) {
    textStyle = styles.txMedium;
  }

  if (props.txSmall) {
    textStyle = styles.txSmall;
  }

  return (
    <View style={[styles.container, containerStyle, borderStyle]}>
      <Text style={[styles.text, textStyle]}>{props.text}</Text>
    </View>
  );
};

export default Subtitle;
