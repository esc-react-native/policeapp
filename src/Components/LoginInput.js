import React, {Component} from 'react';
import {View, TextInput, Text} from 'react-native';
import {Metrics, Fonts, Colors} from '../Styles';
import styles from './Styles/LoginStyles';

export default class LoginInput extends Component {
  state = {
    isFocused: false,
    isValue: false,
  };

  handleFocus = () => this.setState({isFocused: true});
  handleBlur = () => this.setState({isFocused: false});

  handleChangeText = text => {
    this.setState({isValue: text.length > 0});
    this.props.onChangeText(text);
  };

  render() {
    const {label, ...props} = this.props;
    const {isFocused, isValue} = this.state;
    const status = isFocused || isValue;

    const labelStyle = {
      position: 'absolute',
      left: 0,
      top: status ? 0 : Metrics.marginVertical,
      fontSize: status ? Fonts.size.h7 : Fonts.size.h5,
      color: status ? Colors.grayLighter : Colors.grayLight,
    };

    return (
      <View style={styles.inputContainer}>
        <Text style={[styles.inputLabel, labelStyle, this.props.styles]}>
          {label}
        </Text>
        <TextInput
          {...props}
          style={[styles.input, this.props.styles]}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          onChangeText={text => this.handleChangeText(text)}
        />
      </View>
    );
  }
}
