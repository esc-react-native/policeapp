import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Text} from 'react-native';
import styles from './Styles/FormButtonStyles';

export default class FormButton extends Component {
  static propTypes = {
    text: PropTypes.string,
    onPress: PropTypes.func,
    styles: PropTypes.object,
    disabled: PropTypes.bool,
  };

  render() {
    return (
      <TouchableOpacity
        style={[
          styles.button,
          this.props.styles,
          this.props.disabled ? styles.buttonOff : styles.buttonOn,
        ]}
        onPress={this.props.onPress}
        disabled={this.props.disabled}>
        <Text style={[styles.buttonText, this.props.styles]}>
          {this.props.text && this.props.text.toUpperCase()}
        </Text>
      </TouchableOpacity>
    );
  }
}