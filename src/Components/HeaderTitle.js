import React from 'react';
import {View, Text} from 'react-native';
import styles from './Styles/HeaderStyles';

const HeaderTitle = props => {
  return (
    <View style={styles.titleContainer}>
      <Text style={[styles.title, props.styles]}>{props.title}</Text>
      <Text style={[styles.subtitle, props.styles]}>{props.subtitle}</Text>
    </View>
  );
};

export default HeaderTitle;
