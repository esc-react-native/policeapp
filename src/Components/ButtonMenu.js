import React from 'react';
import {View, Image, TouchableOpacity, Text} from 'react-native';
import styles from './Styles/ButtonMenu';
import {Images} from '../Styles';
import Icon from 'react-native-vector-icons/FontAwesome';

const ButtonMenu = props => {
  let ctStyle;
  let txStyle;
  let icStyle;
  let iconImage;

  if (props.small) {
    ctStyle = styles.containerSmall;
    txStyle = styles.textSmall;
    iconImage = (
      <View style={[styles.imageContainer, styles.imageContainerSmall]}>
        <Icon
          name={props.icon}
          size={styles.imageSmall.width / 2}
          color="#FFF"
        />
      </View>
    );
  } else {
    if (props.icon === 'vehicle-plate') {
      iconImage = (
        <View style={[styles.imageContainer]}>
          <Image source={Images.vehicle_plate} style={styles.icon} />
        </View>
      );
    } else {
      iconImage = (
        <View style={[styles.imageContainer]}>
          <Icon name={props.icon} size={styles.imageSmall.width} color="#FFF" />
        </View>
      );
    }
  }

  return (
    <View style={[styles.container, ctStyle]}>
      <TouchableOpacity
        style={[styles.button, props.styles]}
        onPress={props.onPress}
        disabled={props.disabled}>
        {iconImage}
        <Text style={[styles.text, txStyle]}>{props.text}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ButtonMenu;
