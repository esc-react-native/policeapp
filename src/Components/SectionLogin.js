import React from 'react';
import {Image, View} from 'react-native';
import {Images} from '../Styles';
import styles from './Styles/LoginStyles';

const SectionLogin = props => {
  return (
    <View>
      <View style={styles.sectionImage}>
        <Image source={Images.background} style={styles.background} />
        <Image source={Images.logoLight} style={styles.logo} />
      </View>
    </View>
  );
};

export default SectionLogin;
