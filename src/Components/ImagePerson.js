import React from 'react';
import {View, Image} from 'react-native';
import styles from './Styles/ImagePerson';
import {Images} from '../Styles';

const ImagePerson = props => {
  let containerStyle;
  let imageStyle;

  if (props.full) {
    containerStyle = styles.fullContainer;
    imageStyle = styles.fullImage;
  }

  return (
    <View style={[styles.container, containerStyle]}>
      <Image source={props.image} style={[styles.image, imageStyle]} />
    </View>
  );
};

export default ImagePerson;
