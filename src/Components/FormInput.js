import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {TextInput} from 'react-native';
import styles from '../Components/Styles/FormInputStyles';

export default class FormInputText extends Component {
  static propTypes = {
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
    value: PropTypes.string,
    styles: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <TextInput
        style={[styles.input, this.props.styles]}
        onChangeText={this.props.onChangeText}
        value={this.props.value}
        secureTextEntry={this.props.secureTextEntry || false}
        placeholder={this.props.placeholder || ''}
        props
      />
    );
  }
}
