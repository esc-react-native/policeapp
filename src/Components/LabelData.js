import React from 'react';
import {View, Text} from 'react-native';
import styles from './Styles/LabelData';

const LabelData = props => {
  let textStyle;

  let borderStyle;

  if (props.borderBottom) {
    borderStyle = styles.borderBottom;
  }


  if (props.txSmall) {
    textStyle = styles.textSmall;
  }

  return (
    <View style={[styles.container, borderStyle]}>
      <Text style={styles.label}>{props.label}</Text>
      <Text style={[styles.data, textStyle]}>{props.data}</Text>
    </View>
  );
};

export default LabelData;
