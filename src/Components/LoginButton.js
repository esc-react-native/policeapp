import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import styles from './Styles/LoginStyles';

const LoginButton = props => {
  return (
    <View style={styles.buttonContainer}>
      <TouchableOpacity
        style={[styles.button, props.styles]}
        onPress={props.onPress}
        disabled={props.disabled}>
        <Text
          style={[
            styles.buttonText,
            props.styles,
            props.disabled ? styles.buttonOff : styles.buttonOn,
          ]}>
          {props.text && props.text.toUpperCase()} >
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default LoginButton;
