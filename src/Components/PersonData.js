import React from 'react';
import {View, Text} from 'react-native';
import styles from './Styles/PersonData';
import ImagePerson from './ImagePerson';
import LabelData from './LabelData';
import Subtitle from './Subtitle';

import i18 from '../Localize/I18n';

const PersonData = props => {
  const person = props.data.person;
  let body;

  if (props.full) {
    body = (
      <View style={styles.fullContainer}>
        <ImagePerson full image={person.image} />
        <View style={styles.dataContainer}>
          <Subtitle ctLarge text={person.firstName} />
          <Subtitle ctLarge text={person.lastName} />
          <Subtitle borderBottom ctLarge text={person.personId} />

          <Subtitle borderBottom ctLarge text={i18.t('no-reports')} />

          <Subtitle
            ctLarge
            text={`${i18.t('birth')}: ${person.birthday} ${person.month}`}
          />
          <Subtitle ctLarge txMedium text={person.age} />
          <Subtitle ctLarge txSmall text={person.cityState} />
        </View>
      </View>

    );
  } else {
    body = (
      <View style={styles.photoDataContainer}>
        <ImagePerson image={person.image} />
        <View style={styles.dataContainer}>
          <Subtitle ctMedium text={person.firstName} />
          <Subtitle ctMedium text={person.lastName} />
          <Subtitle ctMedium text={person.personId} />

          <Subtitle
            ctSmall
            text={`${i18.t('birth')}: ${person.birthday} ${person.month}`}
          />
          <Subtitle ctSmall text={person.age} />
          <Subtitle ctSmall text={person.cityState} />
        </View>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      {body}
      <LabelData
        txSmall
        label={i18.t('last-address-known')}
        data={person.lastAddress}
      />
    </View>
  );
};

export default PersonData;
