import React from 'react';
import {View, Text, Image} from 'react-native';
import styles from './Styles/Loading';
import {Images} from '../Styles';
import i18 from '../Localize/I18n';


const Loading = props => {
  return (
    <View style={[styles.container, props.full ? styles.full : styles.single]}>
      {props.full && (
        <Image source={Images.gif_police_car} style={styles.image} />
      )}
      <Text
        style={[styles.text, props.full ? styles.fullText : styles.singleText]}>
        {i18.t('loading')}
      </Text>
    </View>
  );
};

export default Loading;
