import React from 'react';
import {View, Text} from 'react-native';

import styles from './Styles/LoginStyles';

const LoginTitle = props => {
  return (
    <View style={styles.titleContainer}>
      <Text style={[styles.title, props.styles]}>{props.title}</Text>
    </View>
  );
};

export default LoginTitle;
