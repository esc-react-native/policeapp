import React, {Component} from 'react';
import {View, TextInput, Text} from 'react-native';
import {Metrics, Fonts, Colors} from '../Styles';
import styles from './Styles/TextArea';
import ButtonBig from './ButtonBig';

export default class TextArea extends Component {
  state = {
    isFocused: false,
    isValue: false,
    button: false,
    value: '',
  };

  handleFocus = () => this.setState({isFocused: true});
  handleBlur = () => this.setState({isFocused: false});

  handleChangeText = text => {
    this.setState({value: text, isValue: text.length > 0, button: true});
    this.props.onChangeText(text);
  };

  handlePress = event => {
    this.setState({isValue: this.state > 0});
    this.props.onPress(this.state.value);
  };

  render() {
    const {label, ...props} = this.props;
    const {isFocused, isValue} = this.state;
    const status = isFocused || isValue;

    const containerStyle = {
      height: status ? Metrics.homeBodyHeight / 2 : Metrics.homeBodyHeight,
    };

    const labelStyle = {
      marginTop: Metrics.marginVerticalLarge,
    };

    return (
      <View
        style={[
          styles.inputContainer,
          props.full
            ? (styles.fullContainer, containerStyle)
            : styles.singleContainer,
        ]}>
        {status && (
          <Text style={[styles.inputLabel, this.props.styles]}>{label}</Text>
        )}
        <TextInput
          {...props}
          multiline={true}
          numberOfLines={4}
          style={[styles.input, this.props.styles]}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          onChangeText={text => this.handleChangeText(text)}
        />
        {!status && (
          <Text style={[styles.inputLabel, labelStyle, this.props.styles]}>
            {label}
          </Text>
        )}
        {status && (
          <ButtonBig
            text={this.props.buttonText}
            disabled={!this.state.button}
            onPress={() => this.handlePress()}
          />
        )}
      </View>
    );
  }
}
