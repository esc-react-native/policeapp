import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import styles from './Styles/HeaderStyles';
import {Images} from '../Styles';
import i18 from '../Localize/I18n';


const HeaderButton = props => {
  return (
    <View style={styles.buttonContainer}>
      <TouchableOpacity
        style={[styles.button, props.styles]}
        onPress={props.onPress}
        disabled={props.disabled}>
        {props.icon === 'back' && (
          <View style={styles.iconContainer}>
            <Image source={Images.back} style={styles.icon} />
            <Text style={styles.iconText}>{i18.t('back')}</Text>
          </View>
        )}
      </TouchableOpacity>
    </View>
  );
};

export default HeaderButton;
