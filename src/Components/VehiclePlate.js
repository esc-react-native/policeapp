import React from 'react';
import {View, Text} from 'react-native';
import styles from './Styles/VehiclePlate';
import LabelData from './LabelData';
import Subtitle from './Subtitle';

import i18 from '../Localize/I18n';
import ImageVehicle from './ImageVehicle';

const VehiclePlate = props => {
  return (
    <View style={styles.container}>
      <LabelData
        label={i18.t('vehicle-plate')}
        data={props.data.vehiclePlate}
      />
      <LabelData label={i18.t('chasis')} data={props.data.chasis} />
      <ImageVehicle img={props.data.image} />
      <Subtitle borderBottom text={props.data.modelFull} />

      <LabelData borderBottom label={i18.t('status')} data={props.data.status.status} />
    </View>
  );
};

export default VehiclePlate;
