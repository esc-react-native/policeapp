import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../../Styles';

export default StyleSheet.create({
  ...AppStyles.screen,

  container: {
    backgroundColor: Colors.background,
  },

  logo: {
    height: Metrics.sectionHeight / 2,
    width: Metrics.width,
    position: 'absolute',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    left: Metrics.marginHorizontalLarge,
    right: Metrics.marginHorizontalLarge,
    bottom: Metrics.marginVerticalLarge,
  },

  sectionImage: {
    height: Metrics.sectionHeight,
    width: Metrics.width,
  },

  background: {
    height: Metrics.sectionHeight,
    resizeMode: 'stretch',
  },

  subtitle: {
    textAlign: 'center',
    fontSize: Fonts.size.regular,
    color: 'white',
  },

  logoContainer: {
    marginTop: 30,
    height: Metrics.screenHeight * (1 / 4),
    flexDirection: 'row',
    justifyContent: 'center',
  },

  headerSection: {
    alignItems: 'center',
    height: Metrics.screenHeight * (1 / 4),
  },

  error: {
    height: 40,
    textAlign: 'center',
    fontSize: Fonts.size.small,
    color: Colors.error,
  },
});
