import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Header from '../Common/Header';
import Banner from '../../Components/Banner';
import ButtonMenu from '../../Components/ButtonMenu';

import {StackActions, NavigationActions} from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';

// Styles
import styles from './Home.style';

import i18 from '../../Localize/I18n';

export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      user: {
        name: 'John Smith',
        job: 'Lt Transit',
      },
    };

    this.handlePress = this.handlePress.bind();
    this.handleMenu = this.handleMenu.bind();
    this.handleSignout = this.handleSignout.bind();
  }
  render() {
    return (
      <View style={styles.homeContainer}>
        <Header
          menu={true}
          title={this.state.user.name}
          subtitle={this.state.user.job}
          onPress={() => this.handleMenu()}
        />
        <Text>PROFILE COMPONENT</Text>
      </View>
    );
  }
}
