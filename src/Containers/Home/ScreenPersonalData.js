/* eslint-disable react/no-did-mount-set-state */
import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Header from '../Common/Header';
import Loading from '../../Components/Loading';
import VehiclePlate from '../../Components/VehiclePlate';
import PersonData from '../../Components/PersonData';
import ButtonMenu from '../../Components/ButtonMenu';

// Styles
import styles from './Styles/VehiclePlate';

import i18 from '../../Localize/I18n';
import {ScrollView} from 'react-native-gesture-handler';
import { Images } from '../../Styles';


export default class ScreenVehiclePlate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      isSearching: false,

      user: {
        name: 'John Smith',
        job: 'Lt Transit',
      },
    };

    this.handlePress = this.handlePress.bind();
    this.handleBack = this.handleBack.bind();
  }

  componentDidMount() {
    this.setState({isLoading: false});
    return;
  }

  handlePress = () => {
    this.props.navigation.navigate('ReportSituation', {
      SearchType: this.props.navigation.getParam('SearchType', 'NO-ID'),
    });
  };

  handleBack = e => {
    const {navigation} = this.props;
    navigation.goBack(null);
  };

  render() {
    let body;


    const dataSample = {
      vehiclePlate: 'AA-02716',
      chasis: 'SB164ZEB10E003873',
      brand: 'Toyota',
      model: 'Corolla',
      modelFull: 'TOYOTA COROLLA 1996 GRIS',
      year: '1996',
      color: 'Gray',
      image: Images.person_01.vehicle_toyota_corolla_1996_gray,
      status: {
        id: 1,
        status: 'Normal'
      },
      person: {
        firstName: 'JOHN PAUL',
        lastName: 'MAXWELL PRIDE',
        personId: '12345678901',
        birthday: '1990/06/20',
        age: `(39 ${i18.t('years')}, 2 ${i18.t('months')}, 20 ${i18.t('days')})`,
        month: i18.t('july'),
        image: Images.person_01,
        cityState: 'Miami Florida',
        lastAddress: '123 Fake Street Floor, Forrest Building, Section Fourth, Apartment 201, East Wooden Groove, Hollywood - Florida',
      },
    };

    body = this.state.isLoading ? (
      <Loading />
    ) : (
      //Body with Loaded Data
      <ScrollView>
        <PersonData full data={dataSample} />
        <ButtonMenu
          icon="send"
          text={i18.t('report-situation')}
          onPress={() => this.handlePress()}
        />
      </ScrollView>
    );

    return (
      <View style={styles.homeContainer}>
        <Header
          back={true}
          title={this.state.user.name}
          subtitle={this.state.user.job}
          onPress={() => this.handleBack()}
        />
        {body}
      </View>
    );
  }
}
