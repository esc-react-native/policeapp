/* eslint-disable react/no-did-mount-set-state */
import React, {Component} from 'react';
import {View, Alert, Text} from 'react-native';
import Header from '../Common/Header';
import Loading from '../../Components/Loading';
import VehiclePlate from '../../Components/VehiclePlate';
import PersonData from '../../Components/PersonData';
import ButtonMenu from '../../Components/ButtonMenu';
import Subtitle from '../../Components/Subtitle'
import {StackActions, NavigationActions} from 'react-navigation';

// Styles
import styles from './Styles/VehiclePlate';

import i18 from '../../Localize/I18n';
import {ScrollView} from 'react-native-gesture-handler';
import {Images} from '../../Styles';
import FormInputText from '../../Components/FormInput';

export default class ScreenSituationSelect extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      isSearching: false,

      user: {
        name: 'John Smith',
        job: 'Lt Transit',
      },
    };

    this.handlePress = this.handlePress.bind();
    this.handleBack = this.handleBack.bind();
  }

  componentDidMount() {
    this.setState({isLoading: false});
    return;
  }

  handlePress = () => {
    //Process to search and navigate to next screen depending of Props.
  };

  handleBack = e => {
    const {navigation} = this.props;
    navigation.goBack(null);
  };

  // Handler Cancel
  handlerCancel = e => {
    const {navigation} = this.props;
    const resetAction = StackActions.reset({
      key: null,
      index: 0,
      actions: [NavigationActions.navigate({routeName: 'Home'})],
    });
    navigation.dispatch(resetAction);
  };

  alertConfirm = (title, message) => {
    Alert.alert(title, message, [
      {text: 'OK', onPress: () => this.handlerCancel()},
    ]);
  };

  handleApproved = e => {
    this.alertConfirm(i18.t('confirmation'), i18.t('registered'));
  };

  render() {
    let body;

    body = this.state.isLoading ? (
      <Loading />
    ) : (
      //Body with Loaded Data
      <View>
        <FormInputText />
        <Subtitle ctLarge text={i18.t('fine-code-description')} />
        <ScrollView style={{marginBottom: 200}}>
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('resist-inspection')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('stop-broken')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('ilegal-reverse')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('speeding')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('run-red-light')}`}
            onPress={() => this.handleApproved()}
          />
          {/* <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('not-signaling')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('crossing-lines')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('not-make-stop')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('not-seat-belt')}`}
            onPress={() => this.handleApproved()}
          /> */}
          {/* <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('not-using-helmet')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('not-insurance')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('not-license')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('reckless-driving')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('drunk-driving')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('leaving-accident')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('failure-keep-right')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('improper-lane-change')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('unsafe-passing')}`}
            onPress={() => this.handleApproved()}
          /> */}
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('improper-cell-phone-use')}`}
            onPress={() => this.handleApproved()}
          />


          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('doubling-a-queue')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('stop-or-double')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('block-traffic')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('fail-to-respect')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('refuse-to-present')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('driving-without-license')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('circulate-without-inspection')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('not-replace-misplaced')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('driving-without-stop-lights')}`}
            onPress={() => this.handleApproved()}
          />
          <ButtonMenu
            small
            icon="car"
            text={`999 ${i18.t('circulate-plate-wrong')}`}
            onPress={() => this.handleApproved()}
          />
        </ScrollView>
      </View>
    );

    return (
      <View style={styles.homeContainer}>
        <Header
          back={true}
          title={this.state.user.name}
          subtitle={this.state.user.job}
          onPress={() => this.handleBack()}
        />
        {body}
      </View>
    );
  }
}
