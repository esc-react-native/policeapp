/* eslint-disable react/no-did-mount-set-state */
import React, {Component} from 'react';
import {View, Alert} from 'react-native';
import Header from '../Common/Header';
import Loading from '../../Components/Loading';
import ButtonMenu from '../../Components/ButtonMenu';
import {StackActions, NavigationActions} from 'react-navigation';

// Styles
import styles from './Styles/VehiclePlate';

import i18 from '../../Localize/I18n';
import {ScrollView} from 'react-native-gesture-handler';

export default class ScreenReportSituation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      isSearching: false,

      user: {
        name: 'John Smith',
        job: 'Lt Transit',
      },
    };

    this.handlePress = this.handlePress.bind();
    this.handleBack = this.handleBack.bind();
  }

  componentDidMount() {
    this.setState({isLoading: false});
    return;
  }

  handlePress = () => {
    //Process to search and navigate to next screen depending of Props.
    this.props.navigation.navigate('SituationSelect', {
      SearchType: this.props.navigation.getParam('SearchType', 'NO-ID'),
    });
  };

  handleNext = () => {
    //Process to search and navigate to next screen depending of Props.
    this.props.navigation.navigate('CustomDescription', {
      SearchType: this.props.navigation.getParam('SearchType', 'NO-ID'),
    });
  };

  handleBack = e => {
    const {navigation} = this.props;
    navigation.goBack(null);
  };

  // Handler Cancel
  handlerCancel = e => {
    const {navigation} = this.props;
    const resetAction = StackActions.reset({
      key: null,
      index: 0,
      actions: [NavigationActions.navigate({routeName: 'Home'})],
    });
    navigation.dispatch(resetAction);
  };

  alertConfirm = (title, message) => {
    Alert.alert(title, message, [
      {text: 'OK', onPress: () => this.handlerCancel()},
    ]);
  };

  handleApproved = e => {
    this.alertConfirm(i18.t('confirmation'), i18.t('registered'));
  };

  render() {
    let body;

    body = this.state.isLoading ? (
      <Loading />
    ) : (
      //Body with Loaded Data
      <ScrollView>
        <ButtonMenu
          icon="info-circle"
          text={i18.t('trafic-fine')}
          onPress={() => this.handlePress()}
        />
        <ButtonMenu
          icon="info-circle"
          text={i18.t('vehicle-problem')}
          onPress={() => this.handlePress()}
        />
        <ButtonMenu
          icon="info-circle"
          text={i18.t('notify-check')}
          onPress={() => this.handlePress()}
        />
        <ButtonMenu
          icon="info-circle"
          text={i18.t('request-crane')}
          onPress={() => this.handleApproved()}
        />
        <ButtonMenu
          icon="info-circle"
          text={i18.t('request-firetruck')}
          onPress={() => this.handleApproved()}
        />
        <ButtonMenu
          icon="info-circle"
          text={i18.t('request-ambulance')}
          onPress={() => this.handleApproved()}
        />

        <ButtonMenu
          icon="info-circle"
          text={i18.t('other')}
          onPress={() => this.handleNext()}
        />
      </ScrollView>
    );

    return (
      <View style={styles.homeContainer}>
        <Header
          back={true}
          title={this.state.user.name}
          subtitle={this.state.user.job}
          onPress={() => this.handleBack()}
        />
        {body}
      </View>
    );
  }
}
