import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Header from '../Common/Header';
import Banner from '../../Components/Banner';
import ButtonMenu from '../../Components/ButtonMenu';
import {StackActions, NavigationActions} from 'react-navigation';

// Styles
import styles from './Home.style';

import i18 from '../../Localize/I18n';
import AsyncStorage from '@react-native-community/async-storage';

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      user: {
        name: 'John Smith',
        job: 'Lt Transit',
      },
    };

    this.handlePress = this.handlePress.bind();
    this.handleMenu = this.handleMenu.bind();
  }

  asyncStorageRemove = key => {
    try {
      AsyncStorage.removeItem(key);
      console.log('Remove Session True ' + key);
      return true;
    } catch (exception) {
      console.log('Remove Session Error ' + key + '= ' + exception);
      return false;
    }
  };

  //
  handlePress = option => {
    this.props.navigation.navigate('Search', {
      SearchType: option,
    });
  };

  handleMenu = () => {
    this.props.navigation.navigate('DrawerMenu');
  };

  handleSignout = () => {
    this.asyncStorageRemove('sessionToken');

    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'AuthStack',
          params: {},
        }),
      ],
    });
    const goLogin = NavigationActions.navigate({
      routeName: 'Login',
      params: {},
    });
    this.props.navigation.dispatch(resetAction);
    this.props.navigation.dispatch(goLogin);
  };

  render() {
    return (
      <View style={styles.homeContainer}>
        <Header
          menu={true}
          title={this.state.user.name}
          subtitle={this.state.user.job}
          onPress={() => this.handleMenu()}
        />
        <Banner />
        <ButtonMenu
          icon="vehicle-plate"
          text={i18.t('vehicle-plate')}
          onPress={() => this.handlePress('vehicle-plate')}
        />
        <ButtonMenu
          icon="address-card"
          text={i18.t('driving-license')}
          onPress={() => this.handlePress('driving-license')}
        />
        <ButtonMenu
          icon="sign-out"
          text={i18.t('exit')}
          onPress={() => this.handleSignout()}
        />
      </View>
    );
  }
}
