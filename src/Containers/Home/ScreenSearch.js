import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Header from '../Common/Header';
import SearchInput from '../../Components/SearchInput';
// import SearchButton from '../../Components/SearchButton';
import Banner from '../../Components/Banner';
import Loading from '../../Components/Loading';

// Styles
import styles from './Search.style';

import i18 from '../../Localize/I18n';

export default class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      isSearching: false,

      user: {
        name: 'John Smith',
        job: 'Lt Transit',
      },

      searchType: '',

      label: '',
      value: '',
    };

    this.handlePress = this.handlePress.bind();
    this.handleBack = this.handleBack.bind();
  }

  componentDidMount() {
    let searchType = this.props.navigation.getParam('SearchType', 'NO-ID');
    let label = '';
    switch (searchType) {
      case 'vehicle-plate':
      case 'driving-license':
        label = i18.t(searchType);
        break;

      default:
        label = 'NO';
        break;
    }

    this.setState({label, searchType, isLoading: false});
  }

  handleChangeText = value => {
    this.setState(value);
    //Process to search and navigate to next screen depending of Props.
  };

  handlePress = searchType => {
    //Process to search and navigate to next screen depending of Props.
    if (searchType === 'vehicle-plate') {
      this.props.navigation.navigate('VehiclePlate', {
        SearchType: searchType,
      });
    } else {
      this.props.navigation.navigate('PersonalData', {
        SearchType: searchType,
      });
    }
  };

  handleBack = e => {
    const {navigation} = this.props;
    navigation.goBack(null);
  };

  render() {
    let body;
    let searchType = this.props.navigation.getParam('SearchType', 'NO-ID');

    if (this.state.isLoading) {
      body = <Loading />;
    } else {
      body = (
        <View>
          <SearchInput
            full
            label={this.state.label}
            onPress={() => this.handlePress(searchType)}
            onChangeText={() => this.handleChangeText}
            buttonText={i18.t('SEARCH')}
          />
        </View>
      );
    }

    return (
      <View style={styles.homeContainer}>
        <Header
          back={true}
          title={this.state.user.name}
          subtitle={this.state.user.job}
          onPress={() => this.handleBack()}
        />
        <Banner text={i18.t('search')} />
        {body}
      </View>
    );
  }
}
