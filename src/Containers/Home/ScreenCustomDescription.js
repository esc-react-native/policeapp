import React, {Component} from 'react';
import {View, Alert} from 'react-native';
import Header from '../Common/Header';
import TextArea from '../../Components/TextArea';
// import SearchButton from '../../Components/SearchButton';
import Banner from '../../Components/Banner';
import Loading from '../../Components/Loading';
import {StackActions, NavigationActions} from 'react-navigation';

// Styles
import styles from './Search.style';

import i18 from '../../Localize/I18n';

export default class ScreenCustomDescription extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      isSearching: false,

      user: {
        name: 'John Smith',
        job: 'Lt Transit',
      },

      searchType: '',

      label: '',
      value: '',
    };

    this.handlePress = this.handlePress.bind();
    this.handleBack = this.handleBack.bind();
  }

  componentDidMount() {
    let searchType = this.props.navigation.getParam('SearchType', 'NO-ID');

    this.setState({searchType, isLoading: false});
  }

  handleChangeText = value => {
    this.setState(value);
    //Process to search and navigate to next screen depending of Props.
  };

  handleBack = e => {
    const {navigation} = this.props;
    navigation.goBack(null);
  };

  // Handler Cancel
  handlerCancel = e => {
    const {navigation} = this.props;
    const resetAction = StackActions.reset({
      key: null,
      index: 0,
      actions: [NavigationActions.navigate({routeName: 'Home'})],
    });
    navigation.dispatch(resetAction);
  };

  alertConfirm = (title, message) => {
    Alert.alert(title, message, [
      {text: 'OK', onPress: () => this.handlerCancel()},
    ]);
  };

  handlePress = e => {
    this.alertConfirm(i18.t('confirmation'), i18.t('registered'));
  };


  render() {
    let body;
    let searchType = this.props.navigation.getParam('SearchType', 'NO-ID');

    if (this.state.isLoading) {
      body = <Loading />;
    } else {
      body = (
        <View>
          <TextArea
            full
            label={i18.t('fine-code-description')}
            onPress={() => this.handlePress(searchType)}
            onChangeText={() => this.handleChangeText}
            buttonText={i18.t('REGISTER')}
          />
        </View>
      );
    }

    return (
      <View style={styles.homeContainer}>
        <Header
          back={true}
          title={this.state.user.name}
          subtitle={this.state.user.job}
          onPress={() => this.handleBack()}
        />
        {body}
      </View>
    );
  }
}
