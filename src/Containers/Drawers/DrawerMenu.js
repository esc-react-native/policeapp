import React from 'react';
import {Text, View, Button, Image, StyleSheet} from 'react-native';

import {Images} from '../../Styles';

export default class DrawerMenu extends React.Component {
  static navigationOptions = {
    drawerLabel: 'Notifications',
    drawerIcon: ({tintColor}) => (
      <Image source={Images.banner} style={styles.image} />
    ),
  };

  render() {
    return (
      <Button
        onPress={() => this.props.navigation.goBack()}
        title="Go back home"
      />
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24,
  },
});
