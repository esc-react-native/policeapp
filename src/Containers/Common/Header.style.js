import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors, AppStyles} from '../../Styles';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.background,
    width: Metrics.screenWidth,
    height: Metrics.headerHeight,
    flexDirection: 'row',
  },

  error: {
    height: 40,
    textAlign: 'center',
    fontSize: Fonts.size.small,
    color: Colors.error,
  },
});
