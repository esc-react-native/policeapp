import React, {Component} from 'react';
import {View} from 'react-native';
import styles from './Header.style';
import HeaderTitle from '../../Components/HeaderTitle'
import HeaderButton from '../../Components/HeaderButton'

export default class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        {this.props.menu && (
          <HeaderButton icon="menu" onPress={this.props.onPress} />
        )}
        {this.props.back && (
          <HeaderButton icon="back" onPress={this.props.onPress} />
        )}
        <HeaderTitle title={this.props.title} subtitle={this.props.subtitle} />
      </View>
    );
  }
}
