import React, {Component} from 'react';
import {View} from 'react-native';
import LoginTitle from '../../Components/LoginTitle';
import LoginButton from '../../Components/LoginButton';
import FloatingInput from '../../Components/LoginInput';
import SectionLogin from '../../Components/SectionLogin';

// Styles
import styles from './Login.style';

//Multilanguage
import i18 from '../../Localize/I18n';
import AsyncStorage from '@react-native-community/async-storage';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      button: false,
    };

    this.handlePress = this.handlePress.bind();
  }

  validate = () => {
    return this.state.username.length && this.state.password.length;
  };

  //
  handleChangeText = (type, text) => {
    this.setState({[type]: text}, () => {
      this.setState({button: this.validate()});
    });
  };

  handlePress = () => {
    AsyncStorage.setItem('sessionToken', 'A');

    this.props.navigation.navigate('Home');
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.section}>
          <LoginTitle title={i18.t('signin')} />
          <FloatingInput
            label={i18.t('username')}
            onChangeText={text => this.handleChangeText('username', text)}
            value={this.state.username}
          />
          <FloatingInput
            label={i18.t('password')}
            onChangeText={text => this.handleChangeText('password', text)}
            value={this.state.password}
            secureTextEntry
          />
          <LoginButton
            text={i18.t('enter')}
            disabled={!this.state.button}
            onPress={() => this.handlePress()}
          />
        </View>
        <SectionLogin />
      </View>
    );
  }
}

