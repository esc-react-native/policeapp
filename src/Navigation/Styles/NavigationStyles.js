import {StyleSheet} from 'react-native';
import {Colors} from '../../Styles';

export default StyleSheet.create({
  header: {
    backgroundColor: Colors.background,
  },
});
