import {createAppContainer} from 'react-navigation';
import Login from '../Containers/Login';
import Home from '../Containers/Home';

import {createStackNavigator} from 'react-navigation-stack';

import styles from './Styles/NavigationStyles';

// Manifest of possible screens
const PrimaryNav = createStackNavigator(
  {
    //Login: {screen: Login},
    //Home: {screen: Home},
  },
  {
    // Default config for all screens
    headerMode: 'none',
    initialRouteName: 'Login',
    navigationOptions: {
      headerStyle: styles.header,
    },
  },
);

export default createAppContainer(PrimaryNav);
