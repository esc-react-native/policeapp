import {Dimensions, Platform} from 'react-native';

const {width, height} = Dimensions.get('window');

const screenWidth = width < height ? width : height;
const screenHeight = width < height ? height : width;
const screenWidthUnit = screenWidth / 100;
const screenHeightUnit = screenHeight / 100;
const marginHorizontal = screenWidthUnit;
const marginVertical = screenHeightUnit;

const sectionWidth = screenWidth / 2;
const sectionHeight = screenHeight / 2;

const marginFactorNormal = 2;
const marginFactorLarge = 4;
const marginFactorSmall = 1;

const headerHeight = screenHeight * (10 / 100);
// Used via Metrics.baseMargin
const metrics = {
  //Margin
  marginHorizontal: marginHorizontal * marginFactorNormal,
  marginVertical: marginVertical * marginFactorNormal,
  marginHorizontalLarge: marginHorizontal * marginFactorLarge,
  marginHorizontalSmall: marginHorizontal * marginFactorSmall,
  marginVerticalLarge: marginVertical * marginFactorLarge,
  marginVerticalSmall: marginVertical * marginFactorSmall,
  //.Section
  sectionWidth: sectionWidth,
  sectionHeight: sectionHeight,
  sectionHeightUnit: sectionHeight / 10,
  //.
  horizontalLineHeight: 1,
  //.
  screenWidth: screenWidth,
  screenHeight: screenHeight,
  widthUnit: screenWidthUnit,
  heightUnit: screenHeightUnit,
  screenWidthNoMargin: screenWidth - ( marginHorizontal * marginFactorLarge * 2 ),
  homeBodyHeight: screenHeight - headerHeight * 2,

  subtitleWidthMedium: screenWidth * (2 / 3),
  subtitleHeightMedium: headerHeight * (5 / 10),
  subtitleWidthSmall: screenWidth * (2 / 3),
  subtitleHeightSmall: headerHeight * (3 / 10),
  //.
  headerHeight: headerHeight,
  navBarHeight: Platform.OS === 'ios' ? screenHeightUnit * 3 : 0,
  //.
  buttonRadius: screenWidthUnit * 2,
  //.
  icons: {
    tiny: 20,
    small: 30,
    medium: 40,
    large: 50,
    xl: 80,
  },
  //.
  images: {
    small: 20,
    medium: 40,
    large: 60,
    logo: 70,
  }
};

export default metrics;
