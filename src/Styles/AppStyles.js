import Fonts from './Fonts';
import Metrics from './Metrics';
import Colors from './Colors';

const AppStyles = {
  screen: {
    homeContainer: {
      flex: 1,
      backgroundColor: Colors.background,
      paddingTop: Metrics.navBarHeight,
    },

    mainContainer: {
      flex: 1,
      backgroundColor: Colors.transparent,
    },

    backgroundImage: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    },

    container: {
      flex: 1,
      paddingTop: Metrics.marginVertical,
      backgroundColor: Colors.transparent,
    },
    section: {
      margin: Metrics.section,
      padding: Metrics.marginHorizontal,
    },

    sectionText: {
      ...Fonts.style.normal,
      paddingVertical: Metrics.marginVertical,
      color: Colors.text,
      marginVertical: Metrics.marginVerticalSmall,
      textAlign: 'center',
    },

    subtitle: {
      color: Colors.snow,
      padding: Metrics.marginVerticalSmall,
      marginBottom: Metrics.marginVerticalSmall,
      marginHorizontal: Metrics.marginHorizontal,
    },

    titleText: {
      ...Fonts.style.h2,
      color: Colors.text,
    },
  },

  darkLabelContainer: {
    padding: Metrics.marginVerticalSmall,
    paddingBottom: Metrics.marginVertical,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    marginBottom: Metrics.marginHorizontal,
  },

  darkLabel: {
    fontFamily: Fonts.type.bold,
    color: Colors.snow,
  },

  groupContainer: {
    margin: Metrics.marginHorizontal,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },

  sectionTitle: {
    ...Fonts.style.h4,
    color: Colors.bluePolice,
    backgroundColor: Colors.ricePaper,
    padding: Metrics.marginVertical,
    marginTop: Metrics.marginVertical,
    marginHorizontal: Metrics.marginHorizontal,
    borderWidth: 1,
    borderColor: Colors.border,
    alignItems: 'center',
    textAlign: 'center',
  },

  icon: {
    width: Metrics.headerHeight / 2,
    height: Metrics.headerHeight / 2,
  },

  text: {
    color: Colors.snow,
  }
};

export default AppStyles;
