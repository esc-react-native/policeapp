import {Dimensions, Platform} from 'react-native';

const {width, height} = Dimensions.get('window');

const type = {
  base: 'Avenir-Book',
  bold: 'Avenir-Black',
  emphasis: 'HelveticaNeue-Italic',
};

const size = {
  titleBig: 50,
  h1: 40,
  h2: 50,
  h3: 40,
  h4: 30,
  h5: 25,
  h6: 20,
  h7: 13,
  input: 30,
  button: 30,
  regular: 30,
  medium: 40,
  small: 20,
  tiny: 10,
  subtitleSmall: 13,
  subtitleMedium: 20,
  subtitleLarge: 25,
};

const style = {
  titleBig: {
    fontFamily: type.base,
    fontSize: size.titleBig,
    fontWeight: 'bold',
  },

  h1: {
    fontFamily: type.base,
    fontSize: size.h1,
    fontWeight: 'bold',
  },
  h2: {
    fontWeight: 'bold',
    fontSize: size.h2,
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3,
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4,
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5,
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6,
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular,
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium,
  },
};

export default {
  type,
  size,
  style,
};
