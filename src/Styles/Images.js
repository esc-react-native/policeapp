const images = {
  logo: require('../../Assets/Images/Logos/PoliceApp_Dark.png'),
  logoDark: require('../../Assets/Images/Logos/PoliceApp_Dark.png'),
  logoLight: require('../../Assets/Images/Logos/PoliceApp_Light.png'),
  logoSmallDark: require('../../Assets/Images/Logos/Logo_Dark.png'),
  logoSmallLight: require('../../Assets/Images/Logos/Logo_Light.png'),

  clearLogo: require('../../Assets/Images/Logos/Logo_Light.png'),

  launch: require('../../Assets/Images/Icons/icon_police_car3.png'),
  deviceInfo: require('../../Assets/Images/Icons/icon_deviceinfo_light.png'),
  faq: require('../../Assets/Images/Icons/icon_faq_light.png'),
  home: require('../../Assets/Images/Icons/icon_home_light.png'),
  menu: require('../../Assets/Images/Icons/icon_menu_light.png'),
  back: require('../../Assets/Images/Icons/icon_backarrow_light.png'),
  close: require('../../Assets/Images/Icons/icon_close_light.png'),
  banner: require('../../Assets/Images/banner.png'),

  icon_car: require('../../Assets/Images/Icons/icon_car_light.png'),
  icon_license: require('../../Assets/Images/Icons/icon_license_light.png'),

  gif_police_car: require('../../Assets/Gifs/police_car.gif'),

  vehicle_toyota_corolla_1996_gray: require('../../Assets/Images/Vehicles/toyota-corolla-1996-gray.png'),

  person_01: require('../../Assets/Images/Persons/person01.png'),
  person_02: require('../../Assets/Images/Persons/person02.jpeg'),
  person_03: require('../../Assets/Images/Persons/person03.jpg'),
  person_04: require('../../Assets/Images/Persons/person04.jpg'),
  person_05: require('../../Assets/Images/Persons/person05.jpg'),

  vehicle_plate: require('../../Assets/Images/Icons/icon_license_plate.png'),

  background: require('../../Assets/Images/Backgrounds/background4.png'),
  buttonBackground: require('../../Assets/Images/Backgrounds/buttonBackgroundDark.png'),
};

export default images;
