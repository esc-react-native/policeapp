import React, {Component} from 'react';

import AppNavigator from './AppNavigator';
import {createAppContainer} from 'react-navigation';


import i18 from '../src/Localize/I18n';

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  constructor(props){
    super(props);

    i18.config();
  }

  render() {
    return <AppContainer />;
  }
}
