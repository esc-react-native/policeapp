import {createSwitchNavigator, createAppContainer} from 'react-navigation';

import {createStackNavigator} from 'react-navigation-stack';

import AuthLoadingScreen from './AuthLoadingScreen';

import ScreenLogin from '../src/Containers/Login/ScreenLogin';
import ScreenHome from '../src/Containers/Home/ScreenHome';
import ScreenSearch from '../src/Containers/Home/ScreenSearch';
import ScreenVehiclePlate from '../src/Containers/Home/ScreenVehiclePlate';
import ScreenPersonalData from '../src/Containers/Home/ScreenPersonalData';
import ScreenReportSituation from '../src/Containers/Home/ScreenReportSituation';
import ScreenSituationSelect from '../src/Containers/Home/ScreenSituationSelect';
import ScreenCustomDescription from '../src/Containers/Home/ScreenCustomDescription';

import DrawerMenu from '../src/Containers/Drawers/DrawerMenu';

// Application Stack Logued Screens
const AppStack = createStackNavigator(
  {
    Home: ScreenHome,
    Search: ScreenSearch,
    VehiclePlate: ScreenVehiclePlate,
    PersonalData: ScreenPersonalData,
    ReportSituation: ScreenReportSituation,
    SituationSelect: ScreenSituationSelect,
    CustomDescription: ScreenCustomDescription,
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
  },
);

// Login Stack
const AuthStack = createStackNavigator(
  {
    Login: ScreenLogin,
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
  },
);

// Login Stack
const MenuDrawer = createStackNavigator(
  {
    DrawerStack: {screen: DrawerMenu},
  },
  {
    headerMode: 'float',
    navigationOptions: ({navigation}) => ({
      headerStyle: {backgroundColor: '#4C3E54'},
      title: 'Welcome!',
      headerTintColor: 'white',
    }),
  },
);

//
const AppNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
    MenuDrawer: MenuDrawer,
  },
  {
    initialRouteName: 'AuthLoading',
    headerMode: 'none',
  },
);

export default createAppContainer(AppNavigator);
