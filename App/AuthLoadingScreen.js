import React from 'react';
import {View, Platform} from 'react-native';
import Loading from '../src/Components/Loading';
import SplashScreen from 'react-native-splash-screen'

import AsyncStorage from '@react-native-community/async-storage';

export default class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    //const sessionToken = await AsyncStorage.getItem('sessionToken');
    let sessionToken;

    sessionToken = null;

    sessionToken = await AsyncStorage.getItem('sessionToken');
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.

    if (Platform.OS !== 'ios') SplashScreen.hide();
      
    this.props.navigation.navigate(sessionToken ? 'App' : 'Auth');
  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
        <Loading />
      </View>
    );
  }
}
